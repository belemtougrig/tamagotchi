package tamagotchi;

import org.apache.commons.lang3.RandomUtils;

public class Tamagotchi {
    public static final int AGE_LIMIT = 10;

    private String name;
    private int age;
    private int energy;
    private int maxEnergy;
    private int fun;
    private int maxFun;
    private int alertThreshold;

    public Tamagotchi(String name) {
        this.name = name;
        this.age = 0;
        this.energy = RandomUtils.nextInt(3, 6);
        this.maxEnergy = RandomUtils.nextInt(5, 10);
        this.fun = RandomUtils.nextInt(3, 6);
        this.maxFun = RandomUtils.nextInt(5, 10);
        this.alertThreshold = RandomUtils.nextInt(3, 6);
    }

    public void decreaseStats() {
        energy--;
        fun--;
        age++;
    }

    public boolean isAlive() {
        return energy > 0 && age < AGE_LIMIT;
    }

    public boolean isHungry() {
        return energy <= alertThreshold;
    }

    public boolean isBored() {
        return fun <= alertThreshold;
    }

    public void feed() {
        int gainedEnergy = RandomUtils.nextInt(1, 4);
        energy = Math.min(energy + gainedEnergy, maxEnergy);
    }

    public void play() {
        int gainedFun = RandomUtils.nextInt(1, 4);
        fun = Math.min(fun + gainedFun, maxFun);
    }

    public void displayStatus() {
        String status = name + " : ";
        if (isAlive()) {
            if (isHungry()) {
                status += "je suis affamé !";
            } else if (isBored()) {
                status += "je m'ennuie à mourir !";
            } else {
                status += "Tout va bien !";
            }
        } else {
            status += "snif : je fais une dépression, ciao!";
        }
        System.out.println(status);
    }

    public String getName() {
        return name;
    }
}
