package game;

import tamagotchi.Tamagotchi;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class TamagotchiGame {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Entrez le nombre de Tamagoshis désiré !");
        int numTamagotchis = scanner.nextInt();

        List<Tamagotchi> tamagotchis = new ArrayList<>();
        for (int i = 1; i <= numTamagotchis; i++) {
            System.out.print("Entrez le nom du Tamagoshi numéro " + i + " : ");
            String name = scanner.next();
            tamagotchis.add(new Tamagotchi(name));
        }

        for (int turn = 1; turn <= Tamagotchi.AGE_LIMIT; turn++) {
            System.out.println("\n------------ Tour n°" + turn + " -------------\n");

            for (Tamagotchi tamagotchi : tamagotchis) {
                tamagotchi.displayStatus();
            }

            System.out.println("\nNourrir quel Tamagoshi ?");
            for (int i = 0; i < tamagotchis.size(); i++) {
                System.out.println("(" + i + ") " + tamagotchis.get(i).getName());
            }

            System.out.print("Entrez un choix : ");
            int feedChoice = scanner.nextInt();
            if (feedChoice >= 0 && feedChoice < tamagotchis.size()) {
                tamagotchis.get(feedChoice).feed();
                System.out.println(tamagotchis.get(feedChoice).getName() + " : Merci !");
            }

            System.out.println("\nJouer avec quel Tamagoshi ?");
            for (int i = 0; i < tamagotchis.size(); i++) {
                System.out.println("(" + i + ") " + tamagotchis.get(i).getName());
            }

            System.out.print("Entrez un choix : ");
            int playChoice = scanner.nextInt();
            if (playChoice >= 0 && playChoice < tamagotchis.size()) {
                tamagotchis.get(playChoice).play();
                System.out.println(tamagotchis.get(playChoice).getName() + " : On se marre !");
            }
        }

        System.out.println("\n--------- fin de partie !! ----------------\n");
        System.out.println("------------- bilan ------------\n");

        for (Tamagotchi tamagotchi : tamagotchis) {
            System.out.println(tamagotchi.getName() + " a survécu et vous remercie :)");
        }

        scanner.close();
    }
}
